import os
from flask_restx import Resource
from flask import Flask, request
from db.db import DatabaseConnect
from utils.api_params import Parameter, FamilyBudgetApi
from werkzeug.security import generate_password_hash, check_password_hash
from http import HTTPStatus
from utils.functions import create_response
from flask_jwt_extended import create_access_token, get_jwt_identity, jwt_required, JWTManager


app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = os.getenv('JWT_SECRET_KEY')
jwt = JWTManager(app)
api = FamilyBudgetApi(app,
                      title='Family budget API',
                      description='FamilyBudget api endpoints.',
                      version='0.1')



# auth = api.model("Auth_model",
#                        {'Authorization': 'Bearer auth_token'})
@api.route('/create_user')
class CreateUser(Resource):
    @api.parameters(parameters=[Parameter(name='username', description='User name to create.', p_type='str'),
                                Parameter(name='password', description='User name password.', p_type='str')])
    def post(self):
        name = request.args.get('username', None)
        password = request.args.get('password', None)
        if not any([name, password]):
            return 400
        hashed_password = generate_password_hash(password)

        with DatabaseConnect() as conn:
            conn.execute(f"INSERT INTO users (name, password)"
                         f" VALUES('{name}', '{hashed_password}')"
                         f" RETURNING id")
            user_id = conn.fetchall()
            if user_id:
                response = create_response(response=200, user=name, password=None, user_id=user_id)

        return response, HTTPStatus.CREATED


@api.route('/login')
class Login(Resource):

    @api.parameters(parameters=[Parameter(name='username', description='User name to create.', p_type='str'),
                                Parameter(name='password', description='User name password.', p_type='str')])
    def post(self):
        username = request.args.get('username', None)
        password = request.args.get('password', None)
        with DatabaseConnect() as conn:
            try:
                conn.execute(f"SELECT password "
                             f"FROM users WHERE "
                             f"name='{username}'")
                hashed_password = conn.fetchone()[0]
                if check_password_hash(hashed_password, password):
                    conn.execute(f"UPDATE users set is_logged = True where name='{username}'")
                access_token = create_access_token(identity=username)
                response = create_response(response=HTTPStatus.OK, user=username, access_token=access_token)
                return response
            except Exception as exc:
                response = create_response(response=HTTPStatus.UNAUTHORIZED, user=username, access_token=None,
                                           exception=exc)
                return response

# should be checked every time user try to interact with db
@api.route('/protected')
class Protected(Resource):
    #@api.expect(auth)
    @api.parameters(parameters=[Parameter(name='username', description='User name to check', p_type='str')])
    @jwt_required()
    def get(self):
        current_user = get_jwt_identity()
        return {'logged_in_as': current_user}


@api.route('/log_out')
class LogOut(Resource):

    @api.parameters(parameters=[Parameter(name='username',
                                          description='User name which will be logged off if is logged.', p_type='str')])
    def post(self):
        username = request.args.get('username')

        try:
            with DatabaseConnect() as conn:
                conn.execute(f"UPDATE users set is_logged = False where name='{username}'")
        except Exception as exc:
            return 404, HTTPStatus.NOT_FOUND
            


@api.route('/share_budget')
class ShareBudget(Resource):
    @api.parameters(parameters=[Parameter(name='budget_name', description='Budget name to share.', p_type='str'),
                                Parameter(name='user_to_share_with', description='User name.', p_type='str')])
    def post(self):
        pass

@api.route('/delete_budget')
class DeleteBudget(Resource):
    def delete(self):
        pass

if __name__ == '__main__':
    app.run(debug=True)
