import typing
from flask_restx import Api


class Parameter:
    def __init__(self, name: str, p_type: str, description: str = '') -> None:
        self.name = name
        self.description = description
        self.type = p_type


class FamilyBudgetApi(Api):
    def parameters(self, parameters: typing.List[Parameter]):
        parser = self.parser()
        if parameters and isinstance(parameters, list):
            for parameter in parameters:
                if parameter.type == 'json':
                    parser.add_argument(parameter.name, location='json', help=parameter.description)
                else:
                    parser.add_argument(parameter.name, location='args', help=parameter.description)

        return self.expect(parser)
