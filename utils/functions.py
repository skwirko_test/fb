def create_response(**kwargs) -> dict:
    resp = {}
    for k, v in kwargs.items():
        resp[k] = v

    return resp
