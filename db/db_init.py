import os

import psycopg2

from db import DatabaseConnect


def create_db():
    conn = psycopg2.connect(
        database="postgres",
        user=os.getenv('PSQL_ADMIN'),
        password=os.getenv('PSQL_PASSWORD'),
        host=os.getenv('PSQL_HOST'),
        port=os.getenv('PSQL_PORT')
    )
    conn.autocommit = True
    cursor = conn.cursor()
    sql = ''' CREATE database family_budget '''
    cursor.execute(sql)
    conn.close()


def create_and_init_database():
    create_db()
    with DatabaseConnect() as conn:
        conn.execute(''' CREATE TABLE users (
        id SERIAL PRIMARY KEY,
        name varchar(25) NOT NULL,
        password varchar(255) NOT NULL,
        is_logged boolean NOT NULL DEFAULT false,
        UNIQUE(name)
        )''')

        conn.execute(''' CREATE TABLE budget (
        id SERIAL PRIMARY KEY,
        name varchar(255) NOT NULL,
        user_name varchar(255) NOT NULL,
        UNIQUE(name)
        ) ''')

        conn.execute(''' CREATE TABLE expenses (
        id SERIAL PRIMARY KEY,
        value FLOAT NOT NULL,
        name varchar(255) NOT NULL,
        user_name varchar(255) REFERENCES users (name),
        date DATE NOT NULL DEFAULT CURRENT_DATE,
        category varchar(255) NOT NULL,
        budget_name varchar(255) REFERENCES budget (name)
        ) ''')

        conn.execute(''' CREATE TABLE incomes (
        id SERIAL PRIMARY KEY,
        value FLOAT NOT NULL,
        name varchar(255) NOT NULL,
        user_name varchar(255) REFERENCES users (name),
        date DATE NOT NULL DEFAULT CURRENT_DATE,
        category varchar(255) NOT NULL,
        budget_name varchar(255) REFERENCES budget (name)
        ) ''')
        print("Database created.")


if __name__ == '__main__':
    create_and_init_database()
