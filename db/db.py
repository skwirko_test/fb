import psycopg2
import os


class DatabaseConnect:
    def __init__(self):
        self.connection = psycopg2.connect(user=os.getenv('PSQL_ADMIN'), password=os.getenv('PSQL_PASSWORD'),
                                           host=os.getenv('PSQL_HOST'), port=os.getenv('PSQL_PORT'),
                                           database=os.getenv('PSQL_DATABASE'))

    def __enter__(self):
        return self.connection.cursor()

    def __exit__(self, execution_type, execution_value, traceback):
        self.connection.commit()
        self.connection.close()
