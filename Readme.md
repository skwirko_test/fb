Install on ubuntu 20.04+:
1. sudo apt-get install docker-compose
2. sudo apt-get install postgresql

DB init:
1. Fill password and admin then copy and paste it to terminal, to create
db configs.

echo export PSQL_ADMIN=<<new_admin_name>> >> ~/.bashrc

echo export PSQL_PASSWORD=<<new_password>> ~/.bashrc

echo export PSQL_HOST=localhost >> ~/.bashrc

echo export PSQL_PORT=5432 >> ~/.bashrc

echo export PSQL_DATABASE=family_budget >> ~/.bashrc

echo export JWT_SECRET_KEY=<<secret_key>> >> ~/.bashrc

source ~/.bashrc

sudo -u postgres createuser -s -i -d -r -l -w $PSQL_ADMIN

sudo -i -u postgres psql -c "ALTER ROLE $PSQL_ADMIN WITH PASSWORD '$PSQL_PASSWORD';"


